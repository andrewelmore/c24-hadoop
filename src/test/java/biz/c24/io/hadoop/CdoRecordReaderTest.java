package biz.c24.io.hadoop;

/*
 * Copyright 2012 C24 Technologies.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RawLocalFileSystem;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.TaskID;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import biz.c24.io.examples.models.xml.Employee;
import biz.c24.test.UnitTest;

/**
 * Validate the behaviour of the ComplexDataObjectRecordReader
 * 
 * @author Andrew Elmore
 */
@Category(UnitTest.class)
public class CdoRecordReaderTest {
    
    private Configuration conf;
    
    @Before
    public void setup() throws IOException, URISyntaxException {
        conf = new Configuration(false);
        conf.set("fs.default.name", "file:///");

        //setup default fs
        RawLocalFileSystem localfs = new RawLocalFileSystem();
        final FileSystem defaultfs = new LocalFileSystem(localfs);
        defaultfs.initialize(new URI("file:///"), conf);
        FileSystem.addFileSystemForTesting(new URI("file:///"), conf, defaultfs);
        
        conf.set("c24.inputformat.element", "biz.c24.io.examples.models.xml.EmployeeElement");
        conf.set("c24.inputformat.startpattern", ".*<employee .*");
        conf.set("c24.inputformat.stoppattern", ".*/>.*");
    }
    
    @Test
    public void testNoSplit() throws IOException, InterruptedException, URISyntaxException {
        
        ComplexDataObjectRecordReader<Employee> reader = new ComplexDataObjectRecordReader<Employee>();
        
        FileSplit split = new FileSplit(new Path("src/test/resources/employees-3-valid.xml"), 0, 1000, null);
        
        TaskAttemptID taskId = new TaskAttemptID(new TaskID("testJob", 1, true, 1), 1);
        
        TaskAttemptContext context = new TaskAttemptContext(conf, taskId);
        
        reader.initialize(split, context);
        
        // File contains 3 records
        assertTrue(reader.nextKeyValue());
        Employee employee = reader.getCurrentValue();
        assertThat(employee.getFirstName(), is("Andy"));
        
        assertTrue(reader.nextKeyValue());
        employee = reader.getCurrentValue();
        assertThat(employee.getFirstName(), is("Joe"));

        assertTrue(reader.nextKeyValue());
        employee = reader.getCurrentValue();
        assertThat(employee.getFirstName(), is("Greg"));

        assertFalse(reader.nextKeyValue()); 
    }
    
    @Test
    public void testPreElementSplit() throws IOException, InterruptedException, URISyntaxException {
        
        ComplexDataObjectRecordReader<Employee> reader = new ComplexDataObjectRecordReader<Employee>();
        
        FileSplit split = new FileSplit(new Path("src/test/resources/employees-3-valid.xml"), 0, 1, null);
        
        TaskAttemptID taskId = new TaskAttemptID(new TaskID("testJob", 1, true, 1), 1);
        
        TaskAttemptContext context = new TaskAttemptContext(conf, taskId);
        
        reader.initialize(split, context);
        
        // Split contains 0 records
        assertFalse(reader.nextKeyValue());
    }
    
    @Test
    public void testPreElementSplit2() throws IOException, InterruptedException, URISyntaxException {
        
        ComplexDataObjectRecordReader<Employee> reader = new ComplexDataObjectRecordReader<Employee>();
        
        FileSplit split = new FileSplit(new Path("src/test/resources/employees-3-valid.xml"), 1, 2, null);
        
        TaskAttemptID taskId = new TaskAttemptID(new TaskID("testJob", 1, true, 1), 1);
        
        TaskAttemptContext context = new TaskAttemptContext(conf, taskId);
        
        reader.initialize(split, context);
        
        // Split contains 0 records
        assertFalse(reader.nextKeyValue());
    }
    
    @Test
    public void testReadToEndElement() throws IOException, InterruptedException {
        
        ComplexDataObjectRecordReader<Employee> reader = new ComplexDataObjectRecordReader<Employee>();
        
        FileSplit split = new FileSplit(new Path("src/test/resources/employees-3-valid.xml"), 0, 15, null);
        
        TaskAttemptID taskId = new TaskAttemptID(new TaskID("testJob", 1, true, 1), 1);
        
        TaskAttemptContext context = new TaskAttemptContext(conf, taskId);
        
        reader.initialize(split, context);
        
        // Split contains 1 record
        assertTrue(reader.nextKeyValue());
        assertFalse(reader.nextKeyValue());
    }
    
    @Test
    public void testMidElementSplit() throws IOException, InterruptedException, URISyntaxException {
        
        ComplexDataObjectRecordReader<Employee> reader = new ComplexDataObjectRecordReader<Employee>();
        
        FileSplit split = new FileSplit(new Path("src/test/resources/employees-3-valid.xml"), 20, 1000, null);
        
        TaskAttemptID taskId = new TaskAttemptID(new TaskID("testJob", 1, true, 1), 1);
        
        TaskAttemptContext context = new TaskAttemptContext(conf, taskId);
        
        reader.initialize(split, context);
        
        // Split contains 2 records
        assertTrue(reader.nextKeyValue());
        assertTrue(reader.nextKeyValue());
        assertFalse(reader.nextKeyValue());
    }
    
    // Split right on the last character of the first message. Ensure only a single message is parsed
    @Test
    public void testBoundaryEndSplit() throws IOException, InterruptedException, URISyntaxException {
        
        ComplexDataObjectRecordReader<Employee> reader = new ComplexDataObjectRecordReader<Employee>();
        
        FileSplit split = new FileSplit(new Path("src/test/resources/employees-3-valid.xml"), 0, 146, null);
        
        TaskAttemptID taskId = new TaskAttemptID(new TaskID("testJob", 1, true, 1), 1);
        
        TaskAttemptContext context = new TaskAttemptContext(conf, taskId);
        
        reader.initialize(split, context);
        
        // Split contains 1 record
        assertTrue(reader.nextKeyValue());
        assertFalse(reader.nextKeyValue());
    }
    
    // Counterpoint to the previous test. Start parsing from the boundary and ensure we get the remaining 2 records
    @Test
    public void testBoundaryStartSplit() throws IOException, InterruptedException, URISyntaxException {
        
        ComplexDataObjectRecordReader<Employee> reader = new ComplexDataObjectRecordReader<Employee>();
        
        FileSplit split = new FileSplit(new Path("src/test/resources/employees-3-valid.xml"), 146, 1000, null);
        
        TaskAttemptID taskId = new TaskAttemptID(new TaskID("testJob", 1, true, 1), 1);
        
        TaskAttemptContext context = new TaskAttemptContext(conf, taskId);
        
        reader.initialize(split, context);
        
        // Split contains 2 records
        assertTrue(reader.nextKeyValue());
        assertTrue(reader.nextKeyValue());
        assertFalse(reader.nextKeyValue());
    }
}
