package biz.c24.io.hadoop.util;

/*
 * Copyright 2012 C24 Technologies.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.MiniMRCluster;

/**
 * Utility class to set up a pseudo cluster for end-to-end tests
 * 
 * @author Andrew Elmore
 *
 */
public class SimulatedCluster {

    private Path TEST_ROOT_DIR = new Path(System.getProperty("test.data.dir", ".") + "/test_" + Math.abs(new Random().nextLong()));
    private FileSystem localFs;
    private MiniMRCluster mrCluster;
    private Configuration conf;

    public Path writeFile(String name, String data) throws IOException {
        Path file = new Path(TEST_ROOT_DIR + "/" + name);
        localFs.delete(file, false);
        DataOutputStream f = localFs.create(file);
        f.write(data.getBytes());
        f.close();
        return file;
    }

    public String readFile(String name) throws IOException {
        DataInputStream f = localFs.open(new Path(TEST_ROOT_DIR + "/" + name));
        BufferedReader b = new BufferedReader(new InputStreamReader(f));
        StringBuilder result = new StringBuilder();
        String line = b.readLine();
        while (line != null) {
            result.append(line);
            result.append('\n');
            line = b.readLine();
        }
        b.close();
        return result.toString();
    }

    public void copyFile(String src, String dest) throws IOException {
        localFs.copyFromLocalFile(new Path("src/test/resources/" + src),
                new Path(TEST_ROOT_DIR + "/" + dest));
    }

    public void initialise(int numTrackers, int maxDirs) throws IOException {
        File root = new File(TEST_ROOT_DIR.toString());
        if (root.exists()) {
            throw new RuntimeException("Test instance data directory"
                    + root.toString() + " already exists");
        }
        // root.deleteOnExit();

        System.setProperty("hadoop.log.dir", new File(TEST_ROOT_DIR.toString(), "/logs").getAbsolutePath());
        mrCluster = new MiniMRCluster(numTrackers, "file:///", maxDirs);

        conf = mrCluster.createJobConf();
        conf.setInt("mapreduce.job.jvm.numtasks", -1);
        localFs = FileSystem.getLocal(conf);
    }

    public void shutdown() {
        if (mrCluster != null) {
            mrCluster.shutdown();
            mrCluster = null;
        }
    }

    public Configuration getConfiguration() {
        return conf;
    }

    public Path getRootDir() {
        return TEST_ROOT_DIR;
    }

}
