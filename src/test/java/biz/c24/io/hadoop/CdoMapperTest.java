package biz.c24.io.hadoop;

/*
 * Copyright 2012 C24 Technologies.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import biz.c24.io.api.ParserException;
import biz.c24.io.examples.models.xml.Employee;
import biz.c24.test.UnitTest;

/**
 * Validate the behaviour of the ComplexDataObjectMapper
 * 
 * @author Andrew Elmore
 *
 */
@Category(UnitTest.class)
public class CdoMapperTest {
    
    private Mapper<Text, Employee, Text, Text>.Context getMockContext() throws IOException, InterruptedException {
        
        Configuration conf = new Configuration(false);
        conf.setBoolean("c24.inputformat.validate", false);
        
        @SuppressWarnings("unchecked")
        Mapper<Text, Employee, Text, Text>.Context context = mock(Mapper.Context.class);
    
        when(context.getConfiguration()).thenReturn(conf);
        
        when(context.getCurrentKey()).thenReturn(new Text("Dummy Key"));
        when(context.getCurrentValue()).thenReturn(new Employee());
        
        return context;
    }

    @Test
    public void testSuccessfulRun() throws IOException, InterruptedException {

        Mapper<Text, Employee, Text, Text>.Context context = getMockContext();
        
        when(context.nextKeyValue()).thenReturn(true)
                                    .thenReturn(true)
                                    .thenReturn(true)
                                    .thenReturn(false);
        
        ComplexDataObjectMapper<Text, Employee, Text, Text> mapper = new ComplexDataObjectMapper<Text, Employee, Text, Text>();
        
        mapper.setup(context);
        
        mapper.run(context);
        
        verify(context, times(4)).nextKeyValue();
    }
    
    @Test
    public void testDefaultAbortOnParseFailure() throws IOException, InterruptedException {

        Mapper<Text, Employee, Text, Text>.Context context = getMockContext();
        
        when(context.nextKeyValue()).thenReturn(true)
                                    .thenThrow(new ParserException("Dummy Message", "Dummy Node"))
                                    .thenReturn(true)
                                    .thenReturn(false);
        
        ComplexDataObjectMapper<Text, Employee, Text, Text> mapper = new ComplexDataObjectMapper<Text, Employee, Text, Text>();
        
        mapper.setup(context);
        
        try {
            mapper.run(context);
            fail("IOException not thrown");
        } catch(IOException ex) {
            // Expected behaviour
        }
        
        verify(context, times(2)).nextKeyValue();       
    }
    
    @Test
    public void testDefaultAbortOnValidationFailure() throws IOException, InterruptedException {

        Mapper<Text, Employee, Text, Text>.Context context = getMockContext();
        context.getConfiguration().setBoolean("c24.inputformat.validate", true);
        
        when(context.nextKeyValue()).thenReturn(true)
                                    .thenReturn(true)
                                    .thenReturn(true)
                                    .thenReturn(false);
        
        ComplexDataObjectMapper<Text, Employee, Text, Text> mapper = new ComplexDataObjectMapper<Text, Employee, Text, Text>();
        
        mapper.setup(context);
        
        try {
            mapper.run(context);
            fail("IOException not thrown");
        } catch(IOException ex) {
            // Expected behaviour
        }
        
        verify(context, times(1)).nextKeyValue();       
    }
    
    
    
}
