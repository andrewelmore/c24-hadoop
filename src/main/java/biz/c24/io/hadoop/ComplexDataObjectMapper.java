package biz.c24.io.hadoop;

/*
 * Copyright 2012 C24 Technologies.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Mapper;

import biz.c24.io.api.ParserException;
import biz.c24.io.api.data.ComplexDataObject;
import biz.c24.io.api.data.ValidationException;
import biz.c24.io.api.data.ValidationManager;

/**
 * An extension to the standard Hadoop mapreduce Mapper to allow parsing and
 * validation errors to be handled. Set c24.inputformat.validate to true in the
 * Job configuration to enforce validation
 * 
 * @author Andrew Elmore
 * 
 * @param <KEYIN>
 *            Input Key type - will be Text if using
 *            ComplexDataObjectFileInputFormat
 * @param <VALUEIN>
 *            The ComplexDataObject-derived type being parsed
 * @param <KEYOUT>
 *            Output key type - depends on mapping logic
 * @param <VALUEOUT>
 *            Output value type - depends on mapping logic
 */
public class ComplexDataObjectMapper<KEYIN, VALUEIN extends ComplexDataObject, KEYOUT, VALUEOUT>
        extends Mapper<KEYIN, VALUEIN, KEYOUT, VALUEOUT> {

    private ValidationManager validator = null;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

        Configuration job = context.getConfiguration();
        boolean validate = job.getBoolean("c24.inputformat.validate", false);
        if (validate) {
            validator = new ValidationManager();
        }
    }

    /**
     * Called when the iO source encounters a parsing exception. Default
     * behaviour is to throw an IOException which will abort the job. Override
     * this method to implement custom handling of parsing exceptions
     * 
     * @param ex
     *            The parse exception
     * @param context
     *            The Job context
     * @throws IOException
     */
    protected void handleException(KEYIN key, ParserException ex, Context context) throws IOException, InterruptedException {
        // Default behaviour is to abort processing
        throw new IOException(ex);
    }

    /**
     * Called when the validation is enabled an a message fails validation.
     * Default behaviour is to throw an IOException which will abort the job.
     * Override this method to implement custom handling of validation
     * exceptions
     * 
     * @param ex
     *            The parse exception
     * @param context
     *            The Job context
     * @throws IOException
     */
    protected void handleException(KEYIN key, ValidationException ex, Context context) throws IOException, InterruptedException {
        // Default behaviour is to abort processing
        throw new IOException(ex);
    }

    /**
     * Derivative of the standard mapreduce run method to optionally validate
     * parsed objects and to allow custom handling of ParserExceptions &
     * ValidationExceptions
     * 
     * @param context
     * @throws IOException
     */
    @Override
    public void run(Context context) throws IOException, InterruptedException {
        setup(context);

        boolean running = true;
        while (running) {
            try {
                running = context.nextKeyValue();
                if (running) {
                    if (validator != null) {
                        validator.validateByException(context.getCurrentValue());
                    }
                    map(context.getCurrentKey(), context.getCurrentValue(), context);
                }
            } catch (ParserException parseException) {
                handleException(context.getCurrentKey(), parseException, context);
            } catch (ValidationException validateException) {
                handleException(context.getCurrentKey(), validateException, context);
            }
        }

        cleanup(context);
    }
}
