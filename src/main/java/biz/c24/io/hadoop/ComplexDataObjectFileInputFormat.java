package biz.c24.io.hadoop;

/*
 * Copyright 2012 C24 Technologies.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import biz.c24.io.api.data.ComplexDataObject;

/**
 * Parses ComplexDataObject-derived types from files, using the
 * ComplexDataObjectRecordReader; see this type for configuration options. The
 * Key contains as much contextual information for the parsed object as possible
 * The Value is the parsed object (of type T)
 * 
 * @author Andrew Elmore
 * 
 * @param <T>
 *            The ComplexDataObject-derived type that is being parsed.
 */
public class ComplexDataObjectFileInputFormat<T extends ComplexDataObject> extends FileInputFormat<Text, T> {

    @Override
    public RecordReader<Text, T> createRecordReader(InputSplit split, TaskAttemptContext context) 
                throws IOException, InterruptedException {

        return new ComplexDataObjectRecordReader<T>();
    }

    @Override
    protected boolean isSplitable(JobContext context, Path filename) {
        // We can split iff our RecordReader can split the file format and our
        // parent agrees
        return super.isSplitable(context, filename)
                && ComplexDataObjectRecordReader.canSplit(context, filename);
    }

}
