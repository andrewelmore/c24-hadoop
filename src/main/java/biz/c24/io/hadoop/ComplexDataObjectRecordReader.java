package biz.c24.io.hadoop;

/*
 * Copyright 2012 C24 Technologies.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.InvalidJobConfException;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.c24.io.api.data.ComplexDataObject;
import biz.c24.io.api.data.Element;
import biz.c24.io.api.presentation.FIXSource;
import biz.c24.io.api.presentation.Source;
import biz.c24.io.api.presentation.TextualSource;
import biz.c24.io.api.presentation.SwiftSource;
import biz.c24.io.api.presentation.XMLSource;

/**
 * Custom RecordReader to parse ComplexDataObject-derived objects (of type T)
 * from a file in the DFS. Supports InputSplits if a start pattern is defined
 * and the parent RecordReader supports them too. 
 * 
 * Configuration:
 * c24.inputformat.element.      The type to be parsed, e.g. biz.c24.io.fix42.ExecutionReportElement. [Required]
 * c24.inputformat.startpattern. A regular expression used to determine which lines denote the first 
 *                               line of each message, e.g. ^8=FIX.*$ If this parameter is not specified, 
 *                               the file cannot be split [Optional]
 * c24.inputformat.stoppattern.  A regular expression used to determine which lines denote the last line 
 *                               of each message. If specified, when parsing a message further lines 
 *                               matching the startpattern will not trigger the start of a new message [Optional]
 * 
 * There is an important consideration that where lines other than the first
 * line of a message match the startpattern, it is possible that the mapreduce
 * splitting logic will cause them to be detected as a new message. Patterns
 * need to be carefully defined to prevent this causing problems.
 * 
 * @author Andrew Elmore
 * 
 * @param <T>
 *            The ComplexDataObject-derived type being parsed. Corresponds to
 *            the c24.inputformat.element
 */
public class ComplexDataObjectRecordReader<T extends ComplexDataObject> extends RecordReader<Text, T> {

    private static Logger LOG = LoggerFactory.getLogger(ComplexDataObjectRecordReader.class);

    private Element element;
    private Source source;
    private Pattern elementStartPattern;
    private Pattern elementStopPattern;
    private String lineTerminator = null;
    private long startIndex;
    private long endIndex;
    private long currentIndex = 0;
    private BufferedReader reader;
    private FileSplit split;

    private Text key = new Text();
    private T value = null;

    /**
     * Used by InputFormats to determine if this RecordReader supports splitting
     * a file. We can only split a file if we have a elementStartPattern -
     * defined by c24.inputformat.startpattern
     * 
     * @param context
     * @param path
     * @return
     */
    public static boolean canSplit(JobContext context, Path path) {
        // We can split if we have an elementStartPattern
        return context.getConfiguration().get("c24.inputformat.startpattern") != null;
    }

    @Override
    public void initialize(InputSplit inputSplit, TaskAttemptContext context)
            throws IOException, InterruptedException {

        split = (FileSplit) inputSplit;

        Configuration job = context.getConfiguration();

        String elementName = job.get("c24.inputformat.element");
        if (elementName == null) {
            LOG.error("c24.inputformat.element is not specified");
            throw new InvalidJobConfException("c24.inputformat.element is null");
        }
        LOG.debug("Reading elements of type {}", elementName);

        try {
            element = (Element) Class.forName(elementName).newInstance();
            source = element.getModel().source();
            // This needs moved into the base class/interface...
            if (source instanceof FIXSource) {
                ((FIXSource) source).setEndOfDataRequired(false);
            } else if (source instanceof TextualSource) {
                ((TextualSource) source).setEndOfDataRequired(false);
            } else if (source instanceof SwiftSource) {
                ((SwiftSource) source).setEndOfDataRequired(false);
            }
            LOG.debug("Using source {} to read elements", source.getClass().getName());
        } catch (InstantiationException e) {
            throw new InvalidJobConfException(e);
        } catch (IllegalAccessException e) {
            throw new InvalidJobConfException(e);
        } catch (ClassNotFoundException e) {
            throw new InvalidJobConfException(e);
        }

        String startPattern = job.get("c24.inputformat.startpattern");
        if (startPattern != null) {
            LOG.debug("Using elementStartPattern {}", startPattern);
            elementStartPattern = Pattern.compile(startPattern);
        }

        String stopPattern = job.get("c24.inputformat.stoppattern");
        if (stopPattern != null) {
            if (startPattern == null) {
                LOG.error("c24.inputformat.stoppattern can only be used where c24.inputformat.startpattern is also supplied.");
                LOG.warn("Parsing will be serial and splitting is not supported.");
            } else {
                LOG.debug("Using elementStopPattern {}", stopPattern);
                elementStopPattern = Pattern.compile(stopPattern);
            }
        }

        startIndex = split.getStart();
        endIndex = startIndex + split.getLength();
        final Path file = split.getPath();

        // open the file and seek to the start of the split
        FileSystem fs = file.getFileSystem(job);
        FSDataInputStream inputStream = fs.open(split.getPath());

        // We need to avoid reading half a line
        // We achieve this by:
        // When we reach the end of our split, if the end-point is mid-message,
        // we'll keep reading to the end of the message
        // At the start of a Split, if we're not the first split, rewind a
        // character then throw away the next line.
        if (startIndex > 0) {
            LOG.debug("Skipping to start of InputSplit via seek to {}", startIndex);
            startIndex--;
            inputStream.seek(startIndex);
            // Note that this does create a potential problem.
            // If we have both a start & stop pattern defined however other
            // lines within the message also match the start pattern, then 
            // if we seek into the middle of the message we risk classifying 
            // such a line as the start of a message
        }

        currentIndex = startIndex;

        reader = new BufferedReader(new InputStreamReader(inputStream));

        if (startIndex > 0) {
            // We're starting reading mid-file. As we rewound a character
            // earlier we can safely throw away the
            // next line
            readLine();
            // It's possible that if our file has a 2 character EOL sequence, we
            // will have misread it as
            // we've probably just read a partial line. Reset it so that it's
            // calculated properly on the next line
            lineTerminator = null;
        }

        if (elementStartPattern == null) {
            // We need to parse serially. Just plug the reader straight into the source
            source.setReader(reader);
        }

    }

    private static int MAX_MESSAGE_SIZE = 10000;

    /**
     * Reads a line of data from the InputStream, determining the end of line
     * sequence if necessary.
     * 
     * @return
     * @throws IOException
     */
    private String readLine() throws IOException {
        String line = null;
        if (reader.ready()) {

            // Mark the file in case we need to rewind
            reader.mark(MAX_MESSAGE_SIZE);

            if (lineTerminator != null) {
                line = reader.readLine();
            } else {
                // We need to parse the file to determine the line terminator
                // We support \n, \r and \r\n
                StringBuffer buffer = new StringBuffer();
                int curr;
                while (lineTerminator == null) {
                    curr = reader.read();
                    if (curr == -1) {
                        // EOF - we don't know if this is the terminator or not.
                        // Assume not
                        break;
                    } else if (curr == '\n') {
                        lineTerminator = "\n";
                        LOG.debug("Determined line terminator is \\n");
                    } else if (curr == '\r') {
                        // Need to see if we're \r or \r\n
                        // We can safely mark; we're the first line hence no
                        // danger of being asked to reset later on
                        reader.mark(1);
                        curr = reader.read();
                        if (curr == '\n') {
                            lineTerminator = "\r\n";
                            LOG.debug("Determined line terminator is \\r\\n");
                        } else {
                            lineTerminator = "\r";
                            LOG.debug("Determined line terminator is \\r");
                            reader.reset();
                        }
                    } else {
                        buffer.append((char) curr);
                    }
                }

                line = buffer.toString();
            }

            currentIndex += line.length();
            if (lineTerminator != null) {
                currentIndex += lineTerminator.length();
            }
        }
        return line;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {

        long msgStartIndex = currentIndex;

        if (elementStartPattern != null) {
            // Try and read a message from the file
            StringBuffer elementCache = new StringBuffer();

            String line = null;
            boolean inElement = false;
            long lastIndex = currentIndex;

            // Make sure we don't go beyond our end mark unless we're
            // mid-message
            while ((inElement || currentIndex < endIndex)
                    && (line = readLine()) != null) {

                // We look for the start of a new element if either:
                // a) We're not in an element or
                // b) We don't have an elementStopPattern set (if we do and
                // we're in a element, the presence of a line
                // that matches the element start pattern is deemed to still be
                // part of the same element)
                if ((!inElement || elementStopPattern == null)
                        && (elementStartPattern == null || elementStartPattern
                                .matcher(line).matches())) {
                    // We've encountered the start of a new element
                    String message = elementCache.toString();
                    if (message.trim().length() > 0) {
                        // We were already parsing an element; thus we've
                        // finished extracting our element
                        // Rewind the stream...
                        reader.reset();
                        currentIndex = lastIndex;
                        // ...then break so we store what we have already
                        // extracted
                        break;
                    } else {
                        // This is the start of our element. Add it to our
                        // elementCache.
                        inElement = true;
                        msgStartIndex = lastIndex;
                    }
                }

                if (inElement) {
                    // More data for our current element
                    elementCache.append(line);
                    if (lineTerminator != null) {
                        elementCache.append(lineTerminator);
                    }

                    // If we have an elementStopPattern, see if the line matched
                    if (elementStopPattern != null
                            && elementStopPattern.matcher(line).matches()) {
                        // We've encountered the end of the element
                        break;
                    }
                }

                lastIndex = currentIndex;
            }

            String message = elementCache.toString();
            if (message.trim().length() > 0) {
                // We have a message to parse
                source.setReader(new StringReader(message));
                LOG.info("Parsing: {}", message);
                key.set(split.getPath().toString() + "[" + msgStartIndex + "]: " + message);
                value = (T) source.readObject(element);
            } else {
                value = null;
            }
        } else {
            // We can only read serially from the source
            key.set(split.getPath().toString() + "[" + msgStartIndex + "]");
            value = (T) source.readObject(element);
            if (value != null) {
                if (value.getTotalAttrCount() == 0
                        && value.getTotalElementCount() == 0) {
                    value = null;
                }
            }
        }

        if (value != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Text getCurrentKey() throws IOException, InterruptedException {
        return key;
    }

    @Override
    public T getCurrentValue() throws IOException, InterruptedException {
        return value;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        if (endIndex == startIndex) {
            return 0;
        } else {
            return (currentIndex - startIndex)
                    / (float) (endIndex - startIndex);
        }
    }

    @Override
    public void close() throws IOException {
        if (reader != null) {
            reader.close();
            reader = null;
        }
    }

}
